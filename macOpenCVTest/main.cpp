//
//  main.cpp
//  macOpenCVTest
//
//  Created by Hiroshi KATO on 2019/01/06.
//  Copyright © 2019 Hiroshi KATO. All rights reserved.
//

#include <iostream>
//#include <cv.h>
#include <highgui.hpp>
#include <opencv2/opencv.hpp>

using namespace cv;

int main(int argc, const char * argv[]) {
    // insert code here...
    std::cout << "Hello, World!\n";
 
    Mat limg = imread("/Users/hiroshi/Documents/Development/tsukuba/scene1.row3.col1.ppm");
    Mat rimg = imread("/Users/hiroshi/Documents/Development/tsukuba/scene1.row3.col5.ppm");
    imshow("testimage", limg);
    waitKey(0);

    // SGBM test
    int window_size = 3;
    int minDisparity = 0;
    int numDisparities = 32;
    int blockSize = 3;
    int P1 = 8 * 3 * window_size * window_size;
    int P2 =  32 * 3 * window_size * window_size;
    int disp12MaxDiff = 1;
    int preFilterCap = 0;
    int uniquenessRatio = 10;
    int speckleWindowSize = 100;
    int speckleRange = 32;
    cv::Ptr<cv::StereoSGBM> ssgbm = cv::StereoSGBM::create(
                                                           minDisparity,
                                                           numDisparities,
                                                           blockSize,
                                                           P1,
                                                           P2, disp12MaxDiff,
                                                           preFilterCap,
                                                           uniquenessRatio,
                                                           speckleWindowSize , speckleRange, cv::StereoSGBM::MODE_SGBM);
    cv::Mat disparity;  //((cv::MatSize)leftImg.size, CV_16S);
    ssgbm->compute(limg, rimg, disparity);
    
    cv::Mat disparity_map;
    double min, max;
    cv::minMaxLoc(disparity, &min, &max);
    disparity.convertTo(disparity_map, CV_8UC1, 255.0 / (max - min), -255.0 * min / (max - min));
    cv::imshow("result", disparity_map);
    /*
    Mat img(256,256, CV_8UC3, Scalar(100, 100, 100));
    circle(img, Point(128, 128), 100, Scalar(200, 100, 100), 4);
    imshow("image",img);
     */
    waitKey(0);

    return 0;
}
